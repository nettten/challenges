﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Challenges.BinaryDigitCombinations
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Input validations
            if (args.Length == 0)
            {
                Console.WriteLine("Input string is EMPTY!");
                return;
            }

            #endregion

            string inputTemplate = args[0].ToLower();
            char replacedChar = 'x';

            int degree = inputTemplate.Count(c => c == replacedChar);

            int size = (int)Math.Pow(2, degree);

            string[] rangeBinaryRepresentation
                = GetRangeBinaryRepresentation(0, size)
                 .Select(s => s.PadLeft(degree, '0'))
                 .ToArray();

            IList<string> result = new List<string>();

            for (int i = 0; i < size; i++)
            {
                var tempInp = new StringBuilder(inputTemplate);

                for (int j = 0; j < degree; j++)
                {
                    int indexOfx = tempInp.ToString().IndexOf(replacedChar);
                    tempInp[indexOfx] = rangeBinaryRepresentation[i][j];
                }

                result.Add(tempInp.ToString());
            }

            result.ToList().ForEach(Console.WriteLine);

            Console.Read();
        }

        private static IEnumerable<string> GetRangeBinaryRepresentation(int rangeStart, int rangeEnd)
        {
            return Enumerable.Range(rangeStart, rangeEnd)
                .Select(n => Convert.ToString(n, 2));
        }
    }
}
