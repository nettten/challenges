﻿using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace Challenge.WebService.Controllers
{
    public class MainController : ApiController
    {
        /// <summary>
        /// Get SHA256 hash from string
        /// </summary>
        /// <param name="sourceString">Source string</param>
        /// <returns>SHA256 of string</returns>
        [Route("messages/{sourceString}")]
        [HttpGet]
        public IHttpActionResult GetHashFrom(string sourceString)
        {
            return Json(GetSHA256From(sourceString));
        }

        /// <summary>
        /// Get original string from SHA256 hash
        /// </summary>
        /// <param name="hash">SHA256 hash</param>
        /// <returns>Original string</returns>
        [Route("messages/{hash}")]
        [HttpPost]
        public IHttpActionResult GetStringFrom(string hash)
        {
            if (string.IsNullOrWhiteSpace(hash))
            {
                return Content(HttpStatusCode.NotFound, "Hash is EMPTY!");
            }

            return Json(GetDescryptStringFrom(hash));
        }

        #region private methods

        private string GetSHA256From(string sourceString)
        {
            SHA256 sha256 = SHA256.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(sourceString);
            byte[] hash = sha256.ComputeHash(bytes);

            return GetStringFrom(hash);
        }

        private string GetDescryptStringFrom(string SHA256string)
        {
            string code = "134a587848c1cab6";
            string hashType = "sha256";
            string email = "avetisyan.sar@yandex.ru";

            WebRequest request = WebRequest.Create($"http://md5decrypt.net/Api/api.php?hash={SHA256string}&hash_type={hashType}&email={email}&code={code}");
            using (WebResponse response = request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        private string GetStringFrom(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        #endregion 
    }
}
