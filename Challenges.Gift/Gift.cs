﻿namespace Challenges.Gift
{
    /// <summary>
    /// Gift
    /// </summary>
    internal class Gift
    {
        /// <summary>
        /// Gift identification
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gift cost
        /// </summary>
        public decimal Cost { get; set; }

        public override string ToString()
        {
            return $"{Id} {Cost}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Gift gift = obj as Gift;

            return gift.Cost.Equals(Cost) && gift.Id.Equals(Id);
        }
        
        public override int GetHashCode()
        {
            return Id.GetHashCode() * Cost.GetHashCode() * 7;
        }
    }
}
