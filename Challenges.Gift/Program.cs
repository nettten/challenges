﻿using System;
using System.IO;
using System.Linq;

namespace Challenges.Gift
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Input validations
            if (args.Length < 1)
            {
                Console.WriteLine("Max cost is EMPTY!");
                return;
            }

            if (args.Length == 0)
            {
                Console.WriteLine("File path is EMPTY!");
                return;
            }

            string filePath = args[0].Trim();

            if (!File.Exists(filePath))
            {
                Console.WriteLine("File not exist!");
                return;
            }

            bool maxCostParseSuccess = Decimal.TryParse(args[1], out decimal maxCost);

            if (maxCostParseSuccess == false)
            {
                Console.WriteLine("Invalid max. cost! Can`t parse.");
                return;
            }
            #endregion

            char[] giftDelimiters = new[] { ',' };

            try
            {
                var gifts = File.ReadLines(filePath)
                        .Select(l =>
                        {
                            return ParseGiftFromString(l, giftDelimiters);
                        })
                        .ToArray();

                for (int i = gifts.Length - 1; i >= 0; i--)
                {
                    for (int j = gifts.Length - 1; j >= 0; j--)
                    {
                        if (gifts[i].Equals(gifts[j]))
                        {
                            continue;
                        }

                        if (gifts[i].Cost + gifts[j].Cost <= maxCost)
                        {
                            Console.WriteLine($"{gifts[i]}, {gifts[j]}");
                            Console.Read();
                            Environment.Exit(-1);
                        }
                    }
                }

                Console.WriteLine("Not possible.");
                Console.Read();
            }
            catch (Exception e)
            {
                Console.WriteLine("FATAL ERROR:");
                Console.WriteLine(e);
            }

        }

        private static Gift ParseGiftFromString(string giftString, char[] delimiters)
        {
            string[] splitedLine = giftString.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                decimal cost = decimal.Parse(splitedLine[1]);
                string id = splitedLine[0];

                return new Gift { Id = id, Cost = cost };
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
