# README #

### Challenge #1 ###

GET: 
curl https://1b96d7f0.ngrok.io/messages/123

RESULT: A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3

POST:
curl -H "Content-Length:0" -X POST https://1b96d7f0.ngrok.io/messages/A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3

RESULT: 123

### Challenge #2 ###

Call: Challenges.Gift.exe <file_path> <max_cost>

Example: Challenges.Gift.exe test.txt 2000

Algorithm complexity: O(n^m), n - gifts count, m - count of getting gifts

### Challenge #3 ###

Call: Challenges.BinaryDigitCombinations.exe <binary_template>

Example: Challenges.BinaryDigitCombinations.exe 100x11XX

Algorithm complexity: O(2*(2^n)), n - X count 